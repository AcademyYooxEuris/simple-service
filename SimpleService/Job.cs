﻿using Quartz;
using System;

namespace SimpleService
{
    public class Job : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            Console.WriteLine($" Sono le ore => {DateTime.Now.ToLongTimeString()}");
        }
    }
}