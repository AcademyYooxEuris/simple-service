using Quartz;
using Topshelf;
using Topshelf.Quartz;
using Topshelf.Ninject;

namespace SimpleService
{
    class Program
    {
        private const string ServiceName = "MySimpleService";

        static void Main(string[] args)
        {
            HostFactory.Run(c =>
            {
                c.SetServiceName(ServiceName);
                c.SetDisplayName(ServiceName);
                c.SetDescription(ServiceName);
                c.UseNinject(new IocModule());

                c.Service<Service>(s =>
                {
                    s.ConstructUsingNinject();
                    s.WhenStarted(service => service.Start());
                                       
                    s.ScheduleQuartzJob(q =>
                      q.WithJob(() =>
                          JobBuilder.Create<Job>().Build())
                          .AddTrigger(() => TriggerBuilder.Create()
                              .WithSimpleSchedule(b => b
                                  .WithIntervalInSeconds(10)
                                  .RepeatForever())
                              .Build()));

                    s.WhenStopped(service =>
                    {
                        service.Stop();
                    });
                });

                c.StartAutomatically();
                c.RunAsLocalService();
                c.EnableShutdown();
            });
        }
    }
}
