﻿using Ninject.Modules;

namespace SimpleService
{
    internal class IocModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ILogger>().To<ConsoleLogger>();
        }
    }
}