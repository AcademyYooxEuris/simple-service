﻿namespace SimpleService
{
    internal class Service 
    {
        private ILogger _logger;

        public Service(ILogger logger)
        {
            _logger = logger;
        }

        public void Start()
        {
            _logger.Info("Started");
        }

        public void Stop()
        {
            _logger.Info("Stopped");
        }
    }
}